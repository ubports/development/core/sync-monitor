# Lomiri Sync Monitor

Lomiri Sync Monitor manages syncing Evolution calendars on a schedule. When a sync job fails, Lomiri Sync Monitor sends a notification to the user asking them to fix their account.

![lomiri-sync-monitor notification](doc/screenshots/notification.jpg)

![lomiri-sync-monitor reauthentication UI](doc/screenshots/reauthenticate.jpg)

## Troubleshooting

See [Working on the Calendar feature](https://docs.ubports.com/en/latest/systemdev/calendars.html) to learn about Lomiri Sync Monitor's role and how to troubleshoot it.

## Building

Lomiri Sync Monitor can be built and run on your device using Crossbuilder. See [the Crossbuilder repository](https://github.com/ubports/crossbuilder/) to learn more.

## Contributing

You are welcome to propose patches for inclusion in lomiri-sync-monitor. [System software development](https://docs.ubports.com/systemdev) is a good document to help you get started with this process.

Please note that once a build is complete, you will find that `po/lomiri-sync-monitor.pot` has been changed. Please commit this with your changes.
