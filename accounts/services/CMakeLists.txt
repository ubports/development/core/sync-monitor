set(SERVICE_FILES
    generic-caldav.service
    google-caldav.service
    owncloud-caldav.service
    nextcloud-caldav.service
)

gettext_merge_xml_translations("${SERVICE_FILES}")

foreach(SERVICE_FILE ${SERVICE_FILES})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${SERVICE_FILE}
        DESTINATION ${SERVICE_FILE_DIR}
    )
endforeach()
